import telnetlib
import time
from datetime import datetime 

class MKSRGA:
    #todo: cmon make this variable, sheesh
    defaultAddress = "192.168.0.120"
    defaultPort = "10014"
    defaultTimeout = .05
    terminator = "\r\r".encode()
    maxAMU = 80
    rgadict = dict()
    rgadict["ControlName"]="TangoRGA"
    rgadict["ControlVersion"]="1"
    rgadict["FilamentCommand"] = False
    rgadict["FilamentStatus"] = "OFF"
    rgadict["FilamentSelect"] = 2
    rgadict["FilamentInfo"] = "X"
    rgadict["SummaryState"] = "X"
    rgadict["ActiveFilament"] = rgadict["FilamentSelect"]
    rgadict["ExternalTripEnable"] = "X"
    rgadict["ExternalTripMode"] = "Trip"
    rgadict["ExternalTripEnable"] = "Yes"
    rgadict["MaxOnTime"] = 1230
    rgadict["OnTimeRemaining"] = 1230
    rgadict["Trip"] = "No"
    rgadict["Drive"] = "Off"
    rgadict["EmissionTripState"] = "OK"
    rgadict["ExternalTripState"] = "OK"
    rgadict["RVCTripState"] = "OK"
    rgadict["GaugeTripState"] = "OK"
    
    def Start(self):
        self.StartTelnet()
        self.ReadTelnet()
        self.ReadSensor()
        self.SelectSensor()
        time.sleep(2)
        self.Control()
        self.ReadAll()
        #self.AddBarchart()
        self.AddAnalog()
        
    def StartTelnet(self):
        self.RGA = telnetlib.Telnet(host=self.defaultAddress,port=self.defaultPort,timeout=self.defaultTimeout)
    
    def ReadTelnet(self):
        readback = self.RGA.read_until(self.terminator,self.defaultTimeout)
       
        return readback.decode()

    def ReadSensor(self):
        sensorString = self.WriteRead("Sensors")
        print(str(sensorString))
        leftstring = sensorString.find("LM")
        rightstring = sensorString.rfind("  ")
        self.rgadict["Serial"] = sensorString[leftstring:rightstring]
        print(str(self.rgadict["Serial"]))
        
    def SelectSensor(self):
        sensorString = self.WriteRead("Select " + self.rgadict["Serial"])
        print(self.rgadict["Serial"])
        print(str(sensorString))
        leftstring = sensorString.rfind(" ")
        rightstring = sensorString.find("\r\n\r\n\r\r")
        self.rgadict["Status"] = sensorString[(leftstring+1):rightstring]
    
    def WriteRead(self,inputstring):
        self.RGA.write((inputstring+"\r\n").encode())
        response = self.ReadTelnet()
        return response

    def Control(self):
        self.WriteRead("Control " + self.rgadict["ControlName"] + " " + self.rgadict["ControlVersion"])
        self.ReadAll()
        
    def Release(self):
        self.WriteRead("Release")
        self.ReadAll()

    def AddBarchart(self):
        response = self.WriteRead("AddBarchart Bar1 1 " + str(int(self.maxAMU)) + " PeakCenter 3 0 0 0")
        print(str(response))
    
    def AddAnalog(self):
        response = self.WriteRead('')
        print(str(response))
        response = self.WriteRead("AddAnalog Ana1 1 " + str(int(self.maxAMU)) + " 4 3 0 0 0")
        print(str(response))
        response = self.WriteRead('')
        print(str(response))
    
    def FilamentControl(self,onoff=False):
        if onoff:
            print("Enabling Filament!")
            response = self.WriteRead("FilamentControl On")
            print(str(response))
            self.rgadict["FilamentCommand"] = True    
        else:
            print("Disabling Filament!")
            response = self.WriteRead("FilamentControl Off")
            print(str(response))
            self.rgadict["FilamentCommand"] = False
    
    def FilamentStatus(self):
        response = ''
        reading = True
        while reading:
            # grab a whole bunch of responses just in case there's more on the buffer.
            response += self.WriteRead("FilamentInfo")
            check = response.split()
            
            for i in range(len(check)):
                
                if check[i]=="FilamentInfo":
                    self.rgadict["FilamentInfo"] = check[i+1]                         
                elif check[i]=="SummaryState":
                    self.rgadict["SummaryState"] = check[i+1]
                    self.rgadict["FilamentStatus"] =  self.rgadict["SummaryState"]
                elif check[i]=="ActiveFilament":
                    self.rgadict["ActiveFilament"] = check[i+1]                         
                elif check[i]=="ExternalTripEnable":
                    self.rgadict["ExternalTripEnable"] = check[i+1]
                elif check[i]=="ExternalTripMode":
                    self.rgadict["ExternalTripMode"] = check[i+1]  
                elif check[i]=="ExternalTripEnable":
                    self.rgadict["ExternalTripEnable"] = check[i+1]                         
                elif check[i]=="EmissionTripEnable":
                    self.rgadict["EmissionTripEnable"] = check[i+1] 
                elif check[i]=="MaxOnTime":
                    self.rgadict["MaxOnTime"] = check[i+1]  
                elif check[i]=="OnTimeRemaining":
                    self.rgadict["OnTimeRemaining"] = check[i+1]                         
                elif check[i]=="Trip":
                    self.rgadict["Trip"] = check[i+1] 
                elif check[i]=="Drive":
                    self.rgadict["Drive"] = check[i+1]  
                elif check[i]=="EmissionTripState":
                    self.rgadict["EmissionTripState"] = check[i+1]                         
                elif check[i]=="ExternalTripState":
                    self.rgadict["ExternalTripState"] = check[i+1] 
                elif check[i]=="RVCTripState":
                    self.rgadict["RVCTripState"] = check[i+1]  
                elif check[i]=="GaugeTripState":
                    self.rgadict["GaugeTripState"] = check[i+1]
                    #Done with read! Exit the loop.
                    
                    reading = False                                       
        return response
        
        
    def ReadAll(self):
        
        reading = True
        time.sleep(.001)
        response = ''
        while reading:      
            response += self.ReadTelnet()
            if len(response)==0:
                reading=False
            
        print(str(response))
        return response
        
    def CheckForScanDone(self):
        
        reading = True
        response = ''
        newresponse = ''
        repetitions = 0
        while reading:
            repetitions += 1
            newresponse = self.RGA.read_until(self.terminator,self.defaultTimeout).decode()
            #print(str(datetime.now())+str(newresponse))
            response = response + newresponse
            #print(str(newresponse == ''))
            if newresponse =='' and repetitions >=25:
                reading=False
            elif newresponse != '':
                repetitions = 0
            #if response.find("MassReading " + str(int(self.maxAMU))) > -1:
            #    reading = False
        print(str(response))
        response = response.split()
        AMUValue = [0]
        AMUOutput = [0]
        for i in range(len(response)):
            if response[i]=="ZeroReading":
                AMUValue[0] = float(0)
                AMUOutput[0] = float(response[i+2])
            if response[i]=="MassReading":
                if(float(response[i+2]))>0:
                    AMUValue.append(float(response[i+1]))
                    AMUOutput.append(float(response[i+2]))
                else:
                    AMUValue.append(float(response[i+1]))
                    AMUOutput.append(.00000001)
                
        return AMUOutput,AMUValue
        
    def BarScanStart(self):
        if self.rgadict["FilamentStatus"] == False:
            self.FilamentControl(True)
        
        self.WriteRead("ScanAdd Bar1")
        self.WriteRead("ScanStart 1")
        output = self.CheckForScanDone()
        self.WriteRead("ScanStop")
        return output
        
    def AnalogScanStart(self):
        if self.rgadict["FilamentStatus"] == False:
            self.FilamentControl(True)
        
        response = self.WriteRead("ScanAdd Ana1")
        print(str(response))
        response = self.WriteRead("ScanStart 1")
        print(str(response))
        output = self.CheckForScanDone()
        response = self.WriteRead("ScanStop")
        print(str(response))
        return output
        
    def AnalogScanStartShifted(self):
        if self.rgadict["FilamentStatus"] == False:
            self.FilamentControl(True)
        
        response = self.WriteRead("ScanAdd Ana1")
        print(str(response))
        response = self.WriteRead("ScanStart 1")
        print(str(response))
        AMUOutput,AMUValue = self.CheckForScanDone()
        response = self.WriteRead("ScanStop")
        print(str(response))
        
        peakvals = AMUOutput[1::4]
        #Shift: it looks like the peak value on our RGA seems to shift by .5 AMU. the quickest and dirtiest way to fix this is to... grab the .5 AMU before it. Ugh. Also looks like I need to prepend the 0 mass val.
        
        peakvals.insert(0,0.00000001)
        return peakvals

    def FilamentSelect(self,filament=1):
        
        
        print("Selecting New Filament!")
        temp = "FilamentSelect " + str(int(filament))
        print(temp)
        aaa = self.WriteRead(temp)
        print(aaa)
        self.rgadict["FilamentSelect"] = int(filament)
        
        return self.rgadict["FilamentSelect"]
        
